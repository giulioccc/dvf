# -*- coding: utf-8 -*-

from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, MapCompose, TakeFirst

from .processors import available, price_int


class DefaultLoader(ItemLoader):
    default_input_processor = MapCompose(str.strip)
    default_output_processor = TakeFirst()

    color_in = MapCompose(str.strip, str.title)

    stock_in = MapCompose(available)

    price_in = MapCompose(price_int)
    listPrice_in = MapCompose(price_int)

    images_in = Identity()
    images_out = Identity()

    variants_in = Identity()
    variants_out = Identity()
