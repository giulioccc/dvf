# -*- coding: utf-8 -*-

import scrapy


class Item(scrapy.Item):
    name = scrapy.Field()
    brandId = scrapy.Field()
    url = scrapy.Field()
    description = scrapy.Field()
    images = scrapy.Field()
    color = scrapy.Field()
    variants = scrapy.Field()


class Variant(scrapy.Item):
    color = scrapy.Field()
    price = scrapy.Field()
    listPrice = scrapy.Field()
    size = scrapy.Field()
    stock = scrapy.Field()
