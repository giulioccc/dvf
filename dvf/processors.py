# -*- coding: utf-8 -*-

import re


def available(tags):
    tags = tags.split()
    if any(('available' == tag.lower() for tag in tags)):
        return 1

    return 0


def price_int(price):
    return int(''.join(re.findall(r'\d', price)))
