# -*- coding: utf-8 -*-

from urllib.parse import parse_qs, urlencode, urlparse


def color_fieldkey(data):
    for key in data:
        if key.endswith('color'):
            return key


def fromcolors(url, colors):
    if not colors:
        yield url

    parsed_url = urlparse(url)
    data = parse_qs(parsed_url.query)
    key = color_fieldkey(data)

    for color in colors:
        data[key] = color
        qs = urlencode(data)
        yield parsed_url._replace(query=qs).geturl()
