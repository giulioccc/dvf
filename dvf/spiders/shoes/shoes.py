# -*- coding: utf-8 -*-

import scrapy

from dvf.items import Item, Variant
from dvf.loaders import DefaultLoader
from .util import fromcolors
from .xpath import ITEMPAGE, ITEMSGRID


class ShoesSpider(scrapy.Spider):
    name = 'shoes'
    allowed_domains = ['dvf.com']
    start_urls = [
        'https://www.dvf.com/shoes-accessories/all-shoes/?start=0&sz=99999&format=page-element'
    ]

    def parse(self, response):
        items = response.xpath(ITEMSGRID['items'])
        for item in items:
            url = item.xpath(ITEMSGRID['itemurl']).get()
            colors = item.xpath(ITEMSGRID['itemcolors']).getall()

            urls = fromcolors(url, colors)

            for url in urls:
                yield scrapy.Request(url=url, callback=self.itempage)

    def itempage(self, response):
        loader = DefaultLoader(item=Item(), response=response)
        loader.add_value('brandId', 'dvf')
        loader.add_value('url', response.url)
        loader.add_xpath('images', ITEMPAGE['images'])

        for variant in self.variants(response):
            loader.add_value('variants', variant)

        details_loader = loader.nested_xpath(ITEMPAGE['full_details'])
        details_loader.add_xpath('name', ITEMPAGE['name'])
        details_loader.add_xpath('description', ITEMPAGE['description'])
        details_loader.add_xpath('color', ITEMPAGE['color'])

        yield loader.load_item()

    def variants(self, response):
        details = response.xpath(ITEMPAGE['full_details'])
        loader = DefaultLoader(item=Variant(), selector=details)
        loader.add_xpath('price', ITEMPAGE['price'])
        loader.add_xpath('listPrice', ITEMPAGE['listPrice'])
        loader.add_xpath('color', ITEMPAGE['color'])
        item = loader.load_item()

        for variant in details.xpath(ITEMPAGE['variants']):
            variant_loader = DefaultLoader(item=Variant(), selector=variant)
            variant_loader.add_xpath('stock', ITEMPAGE['variant_availability'])
            variant_loader.add_xpath('size', ITEMPAGE['variant_size'])
            variant = variant_loader.load_item()
            yield dict(item, **variant)
