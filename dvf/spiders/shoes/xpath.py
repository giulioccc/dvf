# -*- coding: utf-8 -*-

ITEMSGRID = {
    'items': '//ul[@id="search-result-items"]/li[contains(@class,"item")]',
    'itemurl': './div/div[@class="product-image"]/a/@href',
    'itemcolors': './div/div[@class="colors-contain"]//a/@data-colorid',
}

ITEMPAGE = {
    'full_details': '//div[@id="product-content"]',
    'name': './h1[@class="product-overview-title"]/text()',
    'description': './/div[contains(@class,"short-desc")]/p/text()',
    'color': './/span[@class="selectedColorName"]/text()',
    'price': './/span[contains(@class,"product-overview-sales-price")]/text()',
    'listPrice': './/span[contains(@class,"product-overview-original-price")]/text()',
    'variants': './/div[contains(@class,"pdp-size-qty-container")]//div[contains(@class,"pdp-default-size-select")]/div',
    'variant_availability': './@class',
    'variant_size': './div/a/div/text()',
    'images': '//div[@id="pdp-image-container"]//img/@src',
}
