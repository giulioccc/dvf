# -*- coding: utf-8 -*-

from lxml.html import html5parser, tostring
from scrapy.http import HtmlResponse
from scrapy.selector import Selector


class BrokenHtmlDownloaderMiddleware(object):

    def process_response(self, request, response, spider):
        res = HtmlResponse(
            url=response.url,
            body=tostring(html5parser.fromstring(response.body))
        )
        return res
