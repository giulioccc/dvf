# DVF spider

## Instructions for deploy

- Run `pip install shub`
- Edit `scrapinghub.yml` with your project ID and API Key
- Run `shub deploy`

## Instructions for running locally

- Install poetry the recommended way indicated in https://poetry.eustace.io/docs/
- Run `poetry install` (python 3.7)
- Run `scrapy crawl shoes`